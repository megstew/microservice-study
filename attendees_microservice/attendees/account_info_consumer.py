from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_account(ch, method, properties, body):
    account_info = json.loads(body)
    first_name = account_info["first_name"]
    last_name = account_info["last_name"]
    email = account_info["email"]
    is_active = account_info["is_active"]
    updated_string = account_info["updated"]
    updated = datetime.fromisoformat(updated_string)

    if is_active:
        AccountVO.objects.update_or_create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_active=is_active,
            updated=updated,
        )
    else:
        AccountVO.objects.delete(email=email)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange="account_info", queue=queue_name)

        channel.basic_consume(
            queue=queue_name, on_message_callback=update_account, auto_ack=True
        )

        channel.start_consuming()

    except AMQPConnectionError as e:
        print("Could not connect to RabbitMQ:", e)
        time.sleep(2.0)
